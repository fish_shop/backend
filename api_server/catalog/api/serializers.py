from rest_framework import serializers

from catalog.models import Category, Product, ProductImage
from content.api.serializers import RecipeListSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
            'parent_category_id',
            'icon',
            'image',
            'is_header',
            'is_popular',
        )


class ProductImageSerializer(serializers.HyperlinkedModelSerializer):
    original = serializers.CharField(source='image.url')

    class Meta:
        model = ProductImage
        fields = (
            'original',
        )


class ProductListSerializer(serializers.HyperlinkedModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    images = ProductImageSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'slug',
            'categories',
            'images',
            'is_new',
            'is_trendy',
        )

    def to_representation(self, instance):
        data = super(ProductListSerializer, self).to_representation(instance)
        result_data = {
            "amount": instance.price.amount,
            "currency": instance.price_currency,
        }
        data["price"] = result_data
        return data


class RelatedProductListSerializer(serializers.HyperlinkedModelSerializer):
    images = ProductImageSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'slug',
            'images',
            'is_new',
            'is_trendy',
        )


class ProductSerializer(ProductListSerializer):
    recipes = RecipeListSerializer(many=True, read_only=True)
    related_products = RelatedProductListSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ProductListSerializer.Meta.fields + (
            'description',
            'store_conditions',
            'shelf_life',
            'store_temperature',
            'pack_weight',
            'recipes',
            'related_products',
        )
        lookup_field = 'slug'


class FilterCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
        )


class FilterPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'price',
            'price_currency',
        )
