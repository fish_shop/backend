from rest_framework import serializers

from content.models import News, Recipe, Discount, TradePlace

LIST_FIELDS = (
    'id',
    'title',
    'short_description',
    'slug',
    'created_at',
)


class NewsListSerializer(serializers.HyperlinkedModelSerializer):
    image = serializers.CharField(source='image.url')

    class Meta:
        model = News
        fields = LIST_FIELDS + ('image',)


class NewsSerializer(NewsListSerializer):
    class Meta:
        model = News
        # fields = ONE_MODEL_FIELDS
        fields = NewsListSerializer.Meta.fields + (
            'content',
        )
        lookup_field = 'slug'


class RecipeListSerializer(serializers.HyperlinkedModelSerializer):
    image = serializers.CharField(source='image.url')

    class Meta:
        model = Recipe
        fields = LIST_FIELDS + ('image',)


class RecipeSerializer(RecipeListSerializer):
    class Meta:
        model = Recipe
        fields = RecipeListSerializer.Meta.fields + (
            'content',
        )
        lookup_field = 'slug'


class DiscountListSerializer(serializers.HyperlinkedModelSerializer):
    image = serializers.CharField(source='image.url')

    class Meta:
        model = Discount
        fields = LIST_FIELDS + ('image',)


class DiscountSerializer(DiscountListSerializer):
    class Meta:
        model = Discount
        fields = DiscountListSerializer.Meta.fields + (
            'content',
        )
        lookup_field = 'slug'


class TradePlaceListSerializer(serializers.ModelSerializer):

    class Meta:
        model = TradePlace
        fields = (
            'title',
            'address',
            'opening_hours',
            'longitude',
            'latitude',
        )
