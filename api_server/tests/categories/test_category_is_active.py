import pytest
from django.test import RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase

from catalog.api.views import CategoryViewSet
from catalog.factories import CategoryFactory


@pytest.mark.django_db
class CategoryIsActiveTest(APITestCase):

    @staticmethod
    def test_get_categories():
        """
        Test for getting list of category objects
        """
        CategoryFactory.create_batch(5, is_active=False)
        category = CategoryFactory.create()

        request = RequestFactory().get(f'api/categories/')
        category_view = CategoryViewSet.as_view({'get': 'list'})
        response = category_view(request)
        actual = response.data

        expected = [
            {
                'id': category.id,
                'name': category.name,
                'slug': category.slug,
                'parent_category_id': category.parent_category_id,
                "icon": None,
                "image": None,
                "is_header": category.is_header,
                "is_popular": category.is_popular,
            }
        ]

        assert response.status_code == status.HTTP_200_OK
        assert actual == expected
