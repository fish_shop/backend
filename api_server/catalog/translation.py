from modeltranslation.translator import translator, TranslationOptions

from .models import Category, Product


class CategoryTranslationOptions(TranslationOptions):
    fields = () # 'name',
    required_languages = ('ru', 'en')


class ProductTranslationOptions(TranslationOptions):
    fields = () # 'name', 'description',
    required_languages = ('ru', 'en')


translator.register(Category, CategoryTranslationOptions)
translator.register(Product, ProductTranslationOptions)
