import pytest
from django.utils.translation import activate, deactivate
from django.test import RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase

from content.api.views import NewsViewSet
from content.factories import NewsFactory


@pytest.mark.django_db
class NewsTest(APITestCase):
    @staticmethod
    def test_get_all_news():
        news_list = list()

        news_list.append(NewsFactory.create(title='Test1', sort_order=1, image='/media/news/ff/40/17cf9446e-d.jpg'))
        news_list.append(NewsFactory.create(title='Test2', sort_order=10, image='/media/news/ff/40/17cf94e-d.jpg'))
        news_list.append(NewsFactory.create(title='Test3', sort_order=100, image='/media/news/ff/40/17cf9334e-d.jpg'))

        # news_list.append(NewsFactory.create(title_ru='Test1', title_en='Test1', sort_order=1))
        # news_list.append(NewsFactory.create(title_ru='Test2', title_en='Test2', sort_order=10))
        # news_list.append(NewsFactory.create(title_ru='Test3', title_en='Test3', sort_order=100))

        request = RequestFactory().get(f'api/news/')
        news_view = NewsViewSet.as_view({'get': 'list'})
        response = news_view(request)

        actual = response.data
        expected = [
            {
                'id': news.id,
                'title': news.title,
                'short_description': news.short_description,
                'slug': news.slug,
                'created_at': news.created_at.strftime("%d.%m.%Y"),
                'image': news.image.url,
            }
            for news in news_list
        ]

        assert response.status_code == status.HTTP_200_OK
        assert actual == expected

    @staticmethod
    def test_get_one_news():
        news = NewsFactory.create(image='/media/news/ff/40/17c6e-d.jpg')

        request = RequestFactory().get(f'api/news/{news.slug}/')
        news_view = NewsViewSet.as_view({'get': 'retrieve'})
        response = news_view(request, slug=news.slug)

        actual = response.data
        new1 = {
            'id': news.id,
            'title': news.title,
            'short_description': news.short_description,
            'slug': news.slug,
            'created_at': news.created_at.strftime("%d.%m.%Y"),
            'image': news.image.url,
            'content': news.content,
        }

        assert response.status_code == status.HTTP_200_OK
        assert actual == new1

    @staticmethod
    def test_get_multilang_fields():
        news = NewsFactory.create(image='/media/news/ff/40/17c6e-d.jpg')

        activate('ru')
        request = RequestFactory().get(f'api/news/{news.slug}/')
        news_view = NewsViewSet.as_view({'get': 'retrieve'})
        response = news_view(request, slug=news.slug)
        deactivate()

        actual = response.data
        assert actual['title'] == news.title
        assert actual['short_description'] == news.short_description
        assert actual['content'] == news.content

        # assert actual['title'] == news.title_ru
        # assert actual['short_description'] == news.short_description_ru
        # assert actual['content'] == news.content_ru

        activate('en')
        request = RequestFactory().get(f'api/news/{news.slug}/')
        news_view = NewsViewSet.as_view({'get': 'retrieve'})
        response = news_view(request, slug=news.slug)
        deactivate()

        actual = response.data
        assert actual['title'] == news.title
        assert actual['short_description'] == news.short_description
        assert actual['title'] == news.title

        # assert actual['content'] == news.content_en
        # assert actual['short_description'] == news.short_description_en
        # assert actual['content'] == news.content_en
