from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from api_server.settings import MAX_CATEGORIES_DEPTH
from utils import on_transaction_commit
from .models import Category, Product


def _check_nested_categories_depth(category: Category, current_depht: int = 0):
    if current_depht > MAX_CATEGORIES_DEPTH:
        raise RecursionError(f'Nested categories depth is more than {MAX_CATEGORIES_DEPTH}')

    if category.parent_category is not None:
        _check_nested_categories_depth(category.parent_category, current_depht + 1)


def _check_parent_loops(instance: Category):
    category = instance
    while category.parent_category:
        if category.parent_category == instance:
            raise RecursionError('Loops in parent_category are not allowed!')
        category = category.parent_category


def _check_parent_eq_self(instance: Category):
    if instance.parent_category == instance:
        raise RecursionError('Category cant has itself as a parent!')


@receiver(pre_save, sender=Category)
def pre_save_category(instance, **kwargs):
    pre_save.disconnect(pre_save_category, sender=Category)

    try:
        _check_parent_eq_self(instance)
        _check_parent_loops(instance)
        _check_nested_categories_depth(instance)
    finally:
        pre_save.connect(pre_save_category, sender=Category)


def save_all_categories_for_product(instance):
    parents = set()
    categories_to_process = set(instance.categories.all())

    while categories_to_process:
        cat = categories_to_process.pop()
        parents.add(cat)
        if cat.parent_category is not None:
            categories_to_process.add(cat.parent_category)

    instance.categories.set(parents)
    instance.save()


@receiver(post_save, sender=Product)
@on_transaction_commit
def post_save_product(instance, **kwargs):
    post_save.disconnect(post_save_product, sender=Product)

    try:
        save_all_categories_for_product(instance)
    finally:
        post_save.connect(post_save_product, sender=Product)


def post_save_product_for_test(instance, **kwargs):
    post_save.disconnect(post_save_product_for_test, sender=Product)
    try:
        save_all_categories_for_product(instance)
    finally:
        post_save.connect(post_save_product_for_test, sender=Product)
