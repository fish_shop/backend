from rest_framework import viewsets

from content.models import News, Recipe, Discount, TradePlace
from content.api.serializers import NewsSerializer, NewsListSerializer, RecipeSerializer, RecipeListSerializer, \
    DiscountSerializer, DiscountListSerializer, TradePlaceListSerializer


class NewsViewSet(viewsets.ModelViewSet):
    """

    list:
    Get all News objects
    ---

    retrieve:
    Get full information about single News object
    ---
    slug -- A unique string value identifying this News object.

    """
    queryset = News.objects.filter(is_active=True)
    lookup_field = 'slug'
    http_method_names = ['get']

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return NewsSerializer
        return NewsListSerializer


class RecipeViewSet(viewsets.ModelViewSet):
    """

    list:
    Get all Recipe objects
    ---

    retrieve:
    Get full information about single Recipe object
    ---
    slug -- A unique string value identifying this Recipe object.
    """

    queryset = Recipe.objects.filter(is_active=True)
    lookup_field = 'slug'
    http_method_names = ['get']

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return RecipeSerializer
        return RecipeListSerializer


class DiscountViewSet(viewsets.ModelViewSet):
    """

    list:
    Get all Discount objects
    ---

    retrieve:
    Get full information about single Discount object
    ---
    slug -- A unique string value identifying this Discount object.

    """
    queryset = Discount.objects.filter(is_active=True)
    lookup_field = 'slug'
    http_method_names = ['get']

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return DiscountSerializer
        return DiscountListSerializer


class TradePlaceViewSet(viewsets.ModelViewSet):
    """

    list:
    Get all TradePlace objects
    ---

    retrieve:
    Get full information about single TradePlace object
    ---
    slug -- A unique string value identifying this TradePlace object.

    """
    queryset = TradePlace.objects.filter(is_active=True)
    serializer_class = TradePlaceListSerializer
    http_method_names = ['get']


