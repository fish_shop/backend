import pytest
from rest_framework import status
from django.test import RequestFactory

from catalog.api.views import ProductViewSet
from catalog.factories import ProductFactory


@pytest.mark.django_db
@pytest.mark.parametrize('query, count', [
    ('Test0', 3),
    ('Test1', 3),
    ('Test2', 3),
    ('Test3', 2),
    ('Test4', 2),
    ('Super', 1),
])
def test_get_pagination_products(query, count):
    """
    Ensure we can get product objects.
    """

    for i in range(5):
        ProductFactory.create(name='Test' + str(i), description='a')
        ProductFactory.create(name='a', description='Test' + str(i))

        # ProductFactory.create(name_ru='Test' + str(i), description_ru='a')
        # ProductFactory.create(name_ru='a', description_ru='Test' + str(i))

    for i in range(3):
        ProductFactory.create(name='a', description='Test' + str(i))

        # ProductFactory.create(name_ru='a', description_ru='Test' + str(i))

    ProductFactory.create(name='Super', description='Super')

    # ProductFactory.create(name_ru='Super', description_ru='Super')

    url = 'http://0.0.0.0:8060/api/products/?query={0}'.format(query)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    actual = response.data

    assert response.status_code == status.HTTP_200_OK
    assert len(actual) == count
