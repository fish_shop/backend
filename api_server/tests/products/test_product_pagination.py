import pytest
from rest_framework import status
from django.test import RequestFactory

from catalog.api.views import ProductViewSet
from catalog.factories import ProductFactory


@pytest.mark.django_db
@pytest.mark.parametrize('limit, offset, count, previous_link, next_link, results', [
    (
            10, 0, 30, None, 'http://testserver/api/products/?limit=10&offset=10', 10
    ),
    (
            10, 10, 30, 'http://testserver/api/products/?limit=10',
            'http://testserver/api/products/?limit=10&offset=20', 10
    ),
    (
            10, 20, 30, 'http://testserver/api/products/?limit=10&offset=10', None, 10
    ),
    (
            10, 25, 30, 'http://testserver/api/products/?limit=10&offset=15', None, 5
    ),
    (
            10, 30, 30, 'http://testserver/api/products/?limit=10&offset=20', None, 0
    ),
    (
            50, 30, 30, 'http://testserver/api/products/?limit=50', None, 0
    ),
    (
            50, 0, 30, None, None, 30
    ),
])
def test_get_pagination_products(limit, offset, count, previous_link, next_link, results):
    """
    Ensure we can get product objects.
    """
    ProductFactory.create_batch(30)

    url = 'http://0.0.0.0:8060/api/products/?limit={0}&offset={1}'.format(limit, offset)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    actual = response.data

    assert response.status_code == status.HTTP_200_OK
    assert actual["count"] == count
    assert actual["previous"] == previous_link
    assert actual["next"] == next_link
    assert len(actual["results"]) == results
