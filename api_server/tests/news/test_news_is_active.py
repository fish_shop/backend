import pytest
from django.test import RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase

from content.api.views import NewsViewSet
from content.factories import NewsFactory


@pytest.mark.django_db
class NewsTest(APITestCase):
    @staticmethod
    def test_get_all_news():

        NewsFactory.create_batch(3, is_active=False, image='/media/news/ff/40/17cf9446e-d.jpg')

        news_list = list()
        news_list.append(NewsFactory.create(title='Test1', sort_order=10, image='/media/news/ff/40/17cf9446e-d.jpg'))
        news_list.append(NewsFactory.create(title='Test2', sort_order=100, image='/media/news/ff/40/17cf9446e-d.jpg'))

        # news_list.append(NewsFactory.create(title_ru='Test1', title_en='Test1', sort_order=10))
        # news_list.append(NewsFactory.create(title_ru='Test2', title_en='Test2', sort_order=100))

        request = RequestFactory().get(f'api/news/')
        news_view = NewsViewSet.as_view({'get': 'list'})
        response = news_view(request)

        actual = response.data
        expected = [
            {
                'id': _news.id,
                'title': _news.title,
                'short_description': _news.short_description,
                'slug': _news.slug,
                'created_at': _news.created_at.strftime("%d.%m.%Y"),
                'image': _news.image.url,
            }
            for _news in news_list
        ]

        assert response.status_code == status.HTTP_200_OK
        assert actual == expected
