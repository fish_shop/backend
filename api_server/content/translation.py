from modeltranslation.translator import translator, TranslationOptions

from content.models import News, Recipe, Discount


class NewsTranslationOptions(TranslationOptions):
    fields = () # 'title', 'short_description', 'content',
    required_languages = ('ru', 'en')


translator.register(News, NewsTranslationOptions)
translator.register(Recipe, NewsTranslationOptions)
translator.register(Discount, NewsTranslationOptions)
