import factory
from factory.django import DjangoModelFactory

from content.models import News, Recipe, Discount, TradePlace


class AbstractContentFactory(DjangoModelFactory):
    title = factory.Faker('text', locale="ru_RU", max_nb_chars=50, ext_word_list=None)

    # title_ru = factory.Faker('text', locale="ru_RU", max_nb_chars=50, ext_word_list=None)
    # title_en = factory.Faker('text', locale="en_US", max_nb_chars=50, ext_word_list=None)

    short_description = factory.Faker('text', locale="ru_RU", max_nb_chars=1000, ext_word_list=None)

    # short_description_ru = factory.Faker('text', locale="ru_RU", max_nb_chars=1000, ext_word_list=None)
    # short_description_en = factory.Faker('text', locale="en_US", max_nb_chars=1000, ext_word_list=None)

    content = factory.Faker('text', locale="ru_RU", max_nb_chars=2000, ext_word_list=None)

    # content_ru = factory.Faker('text', locale="ru_RU", max_nb_chars=2000, ext_word_list=None)
    # content_en = factory.Faker('text', locale="en_US", max_nb_chars=2000, ext_word_list=None)

    slug = factory.Faker('uuid4', cast_to=str)
    sort_order = factory.Faker('pyint', locale="ru_RU", min_value=100, max_value=9999, step=1)


class NewsFactory(AbstractContentFactory):
    class Meta:
        model = News


class RecipeFactory(AbstractContentFactory):
    class Meta:
        model = Recipe


class DiscountFactory(AbstractContentFactory):
    class Meta:
        model = Discount


class TradePlaceFactory(DjangoModelFactory):
    title = factory.Faker('text', locale="ru_RU", max_nb_chars=50, ext_word_list=None)
    address = factory.Faker('address', locale="ru_RU")
    opening_hours = factory.Faker('text', locale="ru_RU", max_nb_chars=40, ext_word_list=None)
    longitude = factory.Faker('pydecimal', locale="ru_RU", right_digits=6, positive=True, min_value=0, max_value=360)
    latitude = factory.Faker('pydecimal', locale="ru_RU", right_digits=6, positive=True, min_value=0, max_value=360)
    sort_order = factory.Faker('pyint', locale="en_US", min_value=100, max_value=9999, step=1)

    class Meta:
        model = TradePlace
