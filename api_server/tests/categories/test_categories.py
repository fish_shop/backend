import pytest
from django.test import RequestFactory
from django.utils.translation import activate, deactivate
from rest_framework import status
from rest_framework.test import APITestCase

from catalog.api.views import CategoryViewSet
from catalog.factories import CategoryFactory


@pytest.mark.django_db
class CategoryTest(APITestCase):

    @staticmethod
    def test_get_category():
        """
        Test for getting the one category object
        """

        category = CategoryFactory.create()

        request = RequestFactory().get(f'api/categories/{category.id}/')
        category_view = CategoryViewSet.as_view({'get': 'retrieve'})
        response = category_view(request, pk=category.id)
        actual = response.data

        expected = {
            'id': category.id,
            'name': category.name,
            'slug': category.slug,
            'parent_category_id': category.parent_category_id,
            "icon": None,
            "image": None,
            "is_header": category.is_header,
            "is_popular": category.is_popular,
        }
        assert response.status_code == status.HTTP_200_OK
        assert actual == expected

    @staticmethod
    def test_get_categories():
        """
        Test for getting list of category objects
        """
        categories = []

        categories.append(CategoryFactory.create(name='Test1', sort_order=100))
        categories.append(CategoryFactory.create(name='Test2', sort_order=1000))
        categories.append(CategoryFactory.create(name='Test3', sort_order=10000))

        # modeltranslation on

        # categories.append(CategoryFactory.create(name_ru='Test1', name_en='Test1', sort_order=100))
        # categories.append(CategoryFactory.create(name_ru='Test2', name_en='Test2', sort_order=1000))
        # categories.append(CategoryFactory.create(name_ru='Test3', name_en='Test3', sort_order=10000))

        request = RequestFactory().get(f'api/categories/')
        category_view = CategoryViewSet.as_view({'get': 'list'})
        response = category_view(request)
        actual = response.data

        expected = [
            {
                'id': cat.id,
                'name': cat.name,
                'slug': cat.slug,
                'parent_category_id': cat.parent_category_id,
                "icon": None,
                "image": None,
                "is_header": cat.is_header,
                "is_popular": cat.is_popular,
            }
            for cat in categories
        ]
        assert response.status_code == status.HTTP_200_OK
        assert actual == expected

    @staticmethod
    def test_category_multilang_fields():
        """
        1. Создает категорию с мультиязычными полями на всех языках
        2. Для каждого языка устанавлиевает его в локаль и отправляет запрос
        3. Проверяет, что мультиязычные поля отдаются на правильном языке в зав-ти от локали
        """

        category = CategoryFactory.create()

        activate('ru')
        request = RequestFactory().get(f'api/categories/{category.id}/')
        category_view = CategoryViewSet.as_view({'get': 'retrieve'})
        response = category_view(request, pk=category.id)
        deactivate()

        actual = response.data
        assert actual['name'] == category.name

        # modeltranslation on

        # assert actual['name'] == category.name_ru

        activate('en')
        request = RequestFactory().get(f'api/categories/{category.id}/')
        category_view = CategoryViewSet.as_view({'get': 'retrieve'})
        response = category_view(request, pk=category.id)
        deactivate()

        actual = response.data
        assert actual['name'] == category.name

        # modeltranslation on

        # assert actual['name'] == category.name_en
