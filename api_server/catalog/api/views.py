import copy

from django.db.models import Min, Max
from django.contrib.postgres.search import SearchVector
from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import LimitOffsetPagination

from api_server.settings import CATALOG_FILTERS
from catalog.api.serializers import CategorySerializer, ProductSerializer, ProductListSerializer, \
    FilterCategorySerializer, FilterPriceSerializer
from catalog.models import Category, Product
from catalog.api.views_chemas import chema_products, filter_product_chema


class CategoryViewSet(viewsets.ModelViewSet):
    """

    list:
    Get all Category objects
    ---

    retrieve:
    Get full information about single Category object
    ---

    """
    queryset = Category.objects.filter(is_active=True)
    serializer_class = CategorySerializer
    http_method_names = ['get']


class ProductFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name='price', lookup_expr='gte')
    max_price = filters.NumberFilter(field_name='price', lookup_expr='lte')

    class Meta:
        model = Product
        fields = ['min_price', 'max_price', 'is_new', 'is_trendy', ]


class ProductViewSet(viewsets.ModelViewSet):
    """

    list:
    Get all Product objects
    Can filtering products
    ---

    retrieve:
    Get full information about single Product object
    ---
    slug -- A unique string value identifying this Discount object.

    type:
        required: True,
        location: path,

    """
    queryset = Product.objects.filter(is_active=True)
    pagination_class = LimitOffsetPagination
    ordering_fields = ('id', 'name', 'slug', ) #'price', 'sort_order', 'is_new', 'is_trendy', 'store_conditions', 'shelf_life',
                       # 'store_temperature', 'pack_weight'
    lookup_field = 'slug'
    http_method_names = ['get']

    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ProductFilter
    # ordering = ['sort_order', 'name']
    schema = chema_products

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ProductSerializer
        return ProductListSerializer

    def get_queryset(self):
        query = self.request.GET.get('query')

        categories_list = self.request.GET.getlist('categories[]')
        store_temperature_list = self.request.GET.getlist('store_temperature[]')
        shelf_life_list = self.request.GET.getlist('shelf_life[]')
        store_conditions_list = self.request.GET.getlist('store_conditions[]')

        product_list = []

        if query is None or query == '':
            product_list = Product.objects.filter(is_active=True)
        else:
            active_products = Product.objects.filter(is_active=True)
            product_list = active_products.annotate(search=SearchVector('name', 'description')).filter(search=query)

        if not (shelf_life_list == [] or shelf_life_list == ['']):
            product_list = product_list.filter(shelf_life__in=shelf_life_list)

        if not (categories_list == [] or categories_list == ['']):
            product_list = product_list.filter(categories__in=categories_list)

        if not (store_conditions_list == [] or store_conditions_list == ['']):
            product_list = product_list.filter(store_conditions__in=store_conditions_list)

        if not (store_temperature_list == [] or store_temperature_list == ['']):
            product_list = product_list.filter(store_temperature__in=store_temperature_list)

        return product_list


class FilterViewSet(APIView):
    schema = filter_product_chema

    def _get_category_three(self, category: Category) -> dict:
        """
        Метод для одной категории строит структуру дерева категорий с учетом дочерних
        """
        if category is not None:
            category_dict = FilterCategorySerializer(category).data
            category_dict['children'] = [
                self._get_category_three(cat) for cat
                in self._get_categories_list().filter(parent_category=category)
            ]
            return category_dict
        else:
            pass

    @staticmethod
    def _get_categories_list():
        return Category.objects.filter(is_active=True)

    @staticmethod
    def _get_products_list():
        return Product.objects.filter(is_active=True)

    def _get_categories_three(self) -> list:
        root_categories = self._get_categories_list().filter(parent_category=None)
        return [self._get_category_three(cat) for cat in root_categories]

    @staticmethod
    def _set_price_data(data: list) -> dict:
        return {
            'min': data[0],
            'max': data[1],
        }

    def _build_price_filter(self, category: Category, currency=None) -> dict:
        """Return filter price_min, price_max

        Attributes:
        - filter is dictionary thad add 'data'
        - category is Category object

        Example, {'data': {'min': 6, 'max': 100 } }
        """

        if category is not None:
            products = self._get_products_list().filter(categories=category)

        else:
            products = self._get_products_list()
            # Для фильтрации по валюте
            # products = Product.objects.all().filter(price_currency='RUB')

        price_range = products.aggregate(Min('price'), Max('price'))
        price_min, price_max = price_range["price__min"], price_range["price__max"]

        if price_min is not None:
            product_min = self._get_products_list().filter(price=price_min)
            product_max = self._get_products_list().filter(price=price_max)

            _min_price = FilterPriceSerializer(product_min[0])
            _max_price = FilterPriceSerializer(product_max[0])
            return self._set_price_data([_min_price.data, _max_price.data])
        else:
            return self._set_price_data([
                {
                    'price': 0,
                    'price_currency': 'RUB'
                },
                {
                    'price': 100000,
                    'price_currency': 'RUB'

                }
            ])

    def _build_checkbox_filter(self, name: str, category: Category) -> dict:
        """Return filter storage

        Attributes:
        - filter is dictionary thad add 'data'
        - category is Category object

        Example, {'data': [Не указано, 0]}
        """
        if category is not None:
            products = self._get_products_list().filter(categories=category)
        else:
            products = self._get_products_list()

        storage = []
        if name == 'Условия хранения':
            _check_list = products.order_by('store_conditions').distinct('store_conditions')
            for store in _check_list:
                storage.append(store.store_conditions)

        elif name == 'Срок хранения':
            _check_list = products.order_by('shelf_life').distinct('shelf_life')
            for store in _check_list:
                storage.append(store.shelf_life)

        elif name == 'Температура хранения':
            _check_list = products.order_by('store_temperature').distinct('store_temperature')
            for store in _check_list:
                storage.append(store.store_temperature)

        return storage

    def _build_boolean_filter(self, name: str, category: Category) -> dict:
        """Return filter is_new


        Attributes:
        - filter is dictionary thad add 'data'
        - category is Category object


        Example, {'data': True}
        """
        if category is not None:
            products = self._get_products_list().filter(categories=category)
        else:
            products = self._get_products_list()

        bool_value = False
        if name == 'Новинка':
            bool_value = products.filter(is_new=True).exists()

        if name == 'Хит продаж':
            bool_value = products.filter(is_trendy=True).exists()

        return bool_value

    def post(self, request, *args, **kwargs):
        """
        Attributes:
        - currency is 'USD', 'RUB', ... or null
        - category_id is int or null

        If post category_id is int,
        return filters for this
        category and subcategories

        If not post category_id,
        return filters for all
        categories and products

        """

        category_id = request.data.get('category_id')
        currency = request.data.get('currency')

        if category_id is not None:  # Post id of category object
            try:
                category = self._get_categories_list().get(id=category_id)
            except Exception:  # Category with id not exist
                return Response("Not Found")
        else:
            category = None

        result = copy.deepcopy(CATALOG_FILTERS)
        for single_filter in result:
            data = None
            if single_filter['type'] == 'categories':
                data = self._get_categories_three()
            elif single_filter['type'] == 'range':
                data = self._build_price_filter(category, currency)
            elif single_filter['type'] == 'checkbox':
                data = self._build_checkbox_filter(single_filter['name'], category)
            elif single_filter['type'] == 'boolean':
                data = self._build_boolean_filter(single_filter['name'], category)

            single_filter['data'] = data

        return Response(result)
