import copy

import pytest
from django.db.models.signals import post_save
from django.test import RequestFactory
from rest_framework import status

from api_server.settings import CATALOG_FILTERS
from catalog.api.views import FilterViewSet
from catalog.factories import CategoryFactory, ProductFactory
from catalog.models import Product
from catalog.signals import post_save_product_for_test


def _make_list_to_assert_response(
        category_data: list,
        price: list,
        storage: list,
        shelf_life: list,
        store_temperature: list,
        is_new=bool,
        is_trendy=bool
) -> list:
    result = []
    filters = copy.deepcopy(CATALOG_FILTERS)

    for _element in filters:
        if _element['name'] == 'Категории':
            _element["data"] = category_data
            result.append(_element)

        elif _element['name'] == 'Цена':
            if price is None:
                _element["data"] = {
                    'min': {
                        'price': 0,
                        'price_currency': 'RUB',
                    },
                    'max': {
                        'price': 100000,
                        'price_currency': 'RUB',
                    }
                }
            else:
                _element["data"] = {
                    'min': {
                        'price': price[0],
                        'price_currency': 'RUB',
                    },
                    'max': {
                        'price': price[1],
                        'price_currency': 'RUB',
                    }
                }
            result.append(_element)

        if _element['name'] == 'Условия хранения':
            _element["data"] = storage
            result.append(_element)

        if _element['name'] == 'Срок хранения':
            _element["data"] = shelf_life
            result.append(_element)

        if _element['name'] == 'Температура хранения':
            _element["data"] = store_temperature
            result.append(_element)

        if _element['name'] == 'Новинка':
            _element["data"] = is_new
            result.append(_element)

        if _element['name'] == 'Хит продаж':
            _element["data"] = is_trendy
            result.append(_element)
    return result


def _create_category_list():
    categories = list()

    categories.append(CategoryFactory.create(name="Test1", sort_order=10, parent_category=None))

    parent_category = CategoryFactory.create(name="Test2", sort_order=20, parent_category=None)
    categories.append(parent_category)

    categories.append(CategoryFactory.create(name="Test3", sort_order=100,
                                             parent_category=parent_category))
    categories.append(CategoryFactory.create(name="Test4", sort_order=200,
                                             parent_category=parent_category))

    # modeltranslation on

    # categories.append(CategoryFactory.create(name_ru="Test1", name_en="Test1", sort_order=10, parent_category=None))
    #
    # parent_category = CategoryFactory.create(name_ru="Test2", name_en="Test2", sort_order=20, parent_category=None)
    # categories.append(parent_category)
    #
    # categories.append(CategoryFactory.create(name_ru="Test3", name_en="Test3", sort_order=100,
    #                                          parent_category=parent_category))
    # categories.append(CategoryFactory.create(name_ru="Test4", name_en="Test4", sort_order=200,
    #                                          parent_category=parent_category))

    category_data = [
        {
            'id': categories[0].id,
            'name': categories[0].name,
            'slug': categories[0].slug,
            'children': []
        },
        {
            'id': categories[1].id,
            'name': categories[1].name,
            'slug': categories[1].slug,
            'children': [
                {
                    'id': categories[2].id,
                    'name': categories[2].name,
                    'slug': categories[2].slug,
                    'children': []
                }, {
                    'id': categories[3].id,
                    'name': categories[3].name,
                    'slug': categories[3].slug,
                    'children': []
                }
            ]
        }
    ]
    return [categories, category_data]


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (None, False, False, [], [], [], None),
    (None, False, False, [], [], [], 5),
    (None, False, False, [], [], [], 0),
    (None, False, False, [], [], [], 2),
    (None, False, False, [], [], [], 1),
    (None, False, False, [], [], [], 4),
    (None, False, False, [], [], [], 3),
])
def test_category_filters(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    categories = []

    parent_category = CategoryFactory.create(name="Test1", sort_order=10, parent_category=None)
    categories.append(parent_category)

    child_category = CategoryFactory.create(name="Test2", sort_order=100, parent_category=parent_category)
    categories.append(child_category)

    categories.append(
        CategoryFactory.create(name="Test3", sort_order=200, parent_category=parent_category))
    categories.append(
        CategoryFactory.create(name="Test4", sort_order=300, parent_category=child_category))
    categories.append(
        CategoryFactory.create(name="Test5", sort_order=400, parent_category=child_category))
    categories.append(CategoryFactory.create(name="Test6", sort_order=20, parent_category=None))

    # ------------------------------
    # -- modeltranslation
    # ------------------------------

    # parent_category = CategoryFactory.create(name_ru="Test1", name_en="Test1", sort_order=10, parent_category=None)
    # categories.append(parent_category)

    # child_category = CategoryFactory.create(name_ru="Test2", name_en="Test2", sort_order=100,
    #                                         parent_category=parent_category)
    # categories.append(child_category)

    # categories.append(
    #     CategoryFactory.create(name_ru="Test3", name_en="Test3", sort_order=200, parent_category=parent_category))
    # categories.append(
    #     CategoryFactory.create(name_ru="Test4", name_en="Test4", sort_order=300, parent_category=child_category))
    # categories.append(
    #     CategoryFactory.create(name_ru="Test5", name_en="Test5", sort_order=400, parent_category=child_category))
    # categories.append(CategoryFactory.create(name_ru="Test6", name_en="Test6", sort_order=20, parent_category=None))

    category_data = [
        {
            'id': categories[0].id,
            'name': categories[0].name,
            'slug': categories[0].slug,
            'children': [
                {
                    'id': categories[1].id,
                    'name': categories[1].name,
                    'slug': categories[1].slug,
                    'children': [
                        {
                            'id': categories[3].id,
                            'name': categories[3].name,
                            'slug': categories[3].slug,
                            'children': []
                        }, {
                            'id': categories[4].id,
                            'name': categories[4].name,
                            'slug': categories[4].slug,
                            'children': []
                        }
                    ]
                },
                {
                    'id': categories[2].id,
                    'name': categories[2].name,
                    'slug': categories[2].slug,
                    'children': []
                }
            ],
        },
        {
            'id': categories[5].id,
            'name': categories[5].name,
            'slug': categories[5].slug,
            'children': []
        },
    ]

    result = _make_list_to_assert_response(
        category_data=category_data,
        price=price,
        storage=storage,
        shelf_life=shelf_life,
        store_temperature=store_temperature,
        is_new=is_new,
        is_trendy=is_trendy,
    )

    if data is not None:
        data = {'category_id': categories[data].id}

    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (
            ['5.00', '5.00'], False, False, ['Test11', 'Test12', 'Test21', 'Test22',
                                             'Test31', 'Test32', 'Test41', 'Test42'], ['12 месяцев'], ['При -10'], None
    ),
    (
            ['5.00', '5.00'], False, False, ['Test11', 'Test12'], ['12 месяцев'], ['При -10'], 0
    ),
    (
            ['5.00', '5.00'], False, False, ['Test21', 'Test22', 'Test31', 'Test32', 'Test41', 'Test42'],
            ['12 месяцев'], ['При -10'], 1
    ),
    (
            ['5.00', '5.00'], False, False, ['Test31', 'Test32'], ['12 месяцев'], ['При -10'], 2
    ),
    (
            ['5.00', '5.00'], False, False, ['Test41', 'Test42'], ['12 месяцев'], ['При -10'], 3
    ),
])
def test_storage_filter(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    for _category in categories:
        product = ProductFactory.create(price=5, store_conditions=f"{_category.name}1", shelf_life='12 месяцев',
                                        store_temperature='При -10', is_new=False, is_trendy=False)
        product.categories.set([_category])
        product.save()

        product = ProductFactory.create(price=5, store_conditions=f"{_category.name}2", shelf_life='12 месяцев',
                                        store_temperature='При -10', is_new=False, is_trendy=False)
        product.categories.set([_category])
        product.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}
    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (
            ['5.00', '5.00'], False, False, ["В воде"],
            ['Test11', 'Test12', 'Test21', 'Test22', 'Test31', 'Test32', 'Test41', 'Test42'], ['При -10'], None
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['Test11', 'Test12'], ['При -10'], 0
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['Test21', 'Test22', 'Test31', 'Test32', 'Test41', 'Test42'],
            ['При -10'], 1
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['Test31', 'Test32'], ['При -10'], 2
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['Test41', 'Test42'], ['При -10'], 3
    ),
])
def test_shelf_life_filter(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    for _category in categories:
        product = ProductFactory.create(price=5, store_conditions="В воде", shelf_life=f"{_category.name}1",
                                        store_temperature='При -10', is_new=False, is_trendy=False)
        product.categories.set([_category])
        product.save()

        product = ProductFactory.create(price=5, store_conditions="В воде", shelf_life=f"{_category.name}2",
                                        store_temperature='При -10', is_new=False, is_trendy=False)
        product.categories.set([_category])
        product.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}

    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['1 месяц'],
            ['Test11', 'Test12', 'Test21', 'Test22', 'Test31', 'Test32', 'Test41', 'Test42'], None
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['1 месяц'], ['Test11', 'Test12'], 0
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['1 месяц'],
            ['Test21', 'Test22', 'Test31', 'Test32', 'Test41', 'Test42'], 1
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['1 месяц'], ['Test31', 'Test32'], 2
    ),
    (
            ['5.00', '5.00'], False, False, ["В воде"], ['1 месяц'], ['Test41', 'Test42'], 3
    ),
])
def test_store_temperature_filter(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    for _category in categories:
        product = ProductFactory.create(price=5, store_conditions="В воде", store_temperature=f"{_category.name}1",
                                        shelf_life='1 месяц', is_new=False, is_trendy=False)
        product.categories.set([_category])
        product.save()

        product = ProductFactory.create(price=5, store_conditions="В воде", store_temperature=f"{_category.name}2",
                                        shelf_life='1 месяц', is_new=False, is_trendy=False)
        product.categories.set([_category])
        product.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}

    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (['5.00', '5.00'], True, False, ["B"], ['A'], ['C'], None),
    (['5.00', '5.00'], True, False, ["B"], ['A'], ['C'], 0),
    (['5.00', '5.00'], True, False, ["B"], ['A'], ['C'], 1),
    (['5.00', '5.00'], True, False, ["B"], ['A'], ['C'], 2),
    (None, False, False, [], [], [], 3),
])
def test_is_new_filter(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    product = ProductFactory.create(price=5, store_conditions="B", shelf_life='A', store_temperature='C', is_new=True,
                                    is_trendy=False)
    product.categories.set([categories[0]])
    product.save()

    product = ProductFactory.create(price=5, store_conditions="B", shelf_life='A', store_temperature='C', is_new=True,
                                    is_trendy=False)
    product.categories.set([categories[2]])
    product.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}
    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (['5.00', '5.00'], False, True, ["B"], ['A'], ['C'], None),
    (['5.00', '5.00'], False, True, ["B"], ['A'], ['C'], 1),
    (['5.00', '5.00'], False, True, ["B"], ['A'], ['C'], 0),
    (['5.00', '5.00'], False, True, ["B"], ['A'], ['C'], 2),
    (None, False, False, [], [], [], 3),
])
def test_is_trend_filter(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    product = ProductFactory.create(price=5, store_conditions="B", shelf_life='A', store_temperature='C', is_new=False,
                                    is_trendy=True)
    product.categories.set([categories[0]])
    product.save()

    product = ProductFactory.create(price=5, store_conditions="B", shelf_life='A', store_temperature='C', is_new=False,
                                    is_trendy=True)
    product.categories.set([categories[2]])
    product.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}
    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (['50.00', '800.00'], False, False, ["На огне"], ['1 месяц'], ['При -100'], None),
    (['50.00', '800.00'], False, False, ["На огне"], ['1 месяц'], ['При -100'], 1),
    (['50.00', '500.00'], False, False, ["На огне"], ['1 месяц'], ['При -100'], 0),
    (['50.00', '500.00'], False, False, ["На огне"], ['1 месяц'], ['При -100'], 2),
    (['100.00', '800.00'], False, False, ["На огне"], ['1 месяц'], ['При -100'], 3),
])
def test_price_filter(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    for _category in categories[:3]:
        for _price in [50, 500, 300]:
            product = ProductFactory.create(price=_price, store_conditions="На огне", shelf_life="1 месяц",
                                            store_temperature="При -100", is_new=False, is_trendy=False)
            product.categories.set([_category])
            product.save()

    product = ProductFactory.create(price=100, store_conditions="На огне", shelf_life="1 месяц",
                                    store_temperature="При -100", is_new=False, is_trendy=False)
    product.categories.set(categories)
    product.save()

    product = ProductFactory.create(price=800, store_conditions="На огне", shelf_life="1 месяц",
                                    store_temperature="При -100", is_new=False, is_trendy=False)
    product.categories.set([categories[3]])
    product.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}

    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
@pytest.mark.parametrize('price, is_new, is_trendy, storage, shelf_life, store_temperature, data', [
    (
            ['50.00', '800.00'], True, True,
            ['Test', 'Test11', 'Test12', 'Test21', 'Test22', 'Test31', 'Test32', 'В любых условиях'],
            ['12 месяцев', 'Test', 'Test13', 'Test14', 'Test23', 'Test24', 'Test33', 'Test34'],
            ['Test', 'Test15', 'Test16', 'Test25', 'Test26', 'Test35', 'Test36', 'При -15'], None
    ),
    (
            ['50.00', '800.00'], True, True,
            ['Test', 'Test21', 'Test22', 'Test31', 'Test32', 'В любых условиях'],
            ['12 месяцев', 'Test', 'Test23', 'Test24', 'Test33', 'Test34'],
            ['Test', 'Test25', 'Test26', 'Test35', 'Test36', 'При -15'], 1
    ),
    (
            ['50.00', '500.00'], True, True,
            ['Test11', 'Test12', 'В любых условиях'],
            ['12 месяцев', 'Test13', 'Test14'],
            ['Test15', 'Test16', 'При -15'], 0
    ),
    (
            ['50.00', '500.00'], True, True,
            ['Test31', 'Test32', 'В любых условиях'],
            ['12 месяцев', 'Test33', 'Test34'],
            ['Test35', 'Test36', 'При -15'], 2
    ),
    (
            ['100.00', '800.00'], False, False,
            ['Test', 'В любых условиях'],
            ['12 месяцев', 'Test'],
            ['Test', 'При -15'], 3
    ),
])
def test_all_filters(price, is_new, is_trendy, storage, shelf_life, store_temperature, data):
    post_save.connect(post_save_product_for_test, sender=Product)

    _categories_list = _create_category_list()
    categories = _categories_list[0]

    for cat in categories[:3]:
        product1 = ProductFactory.create(price=50, store_conditions=f"{cat.name}1",
                                         shelf_life=f"{cat.name}3", store_temperature=f"{cat.name}5",
                                         is_new=True, is_trendy=True)
        product1.categories.set([cat])
        product1.save()

        product2 = ProductFactory.create(price=500, store_conditions=f"{cat.name}2",
                                         shelf_life=f"{cat.name}4", store_temperature=f"{cat.name}6",
                                         is_new=False, is_trendy=True)
        product2.categories.set([cat])
        product2.save()

    product1 = ProductFactory.create(price=800, store_conditions='Test',
                                     shelf_life='Test', store_temperature='Test',
                                     is_new=False, is_trendy=False)
    product1.categories.set([categories[3]])
    product1.save()

    product2 = ProductFactory.create(price=100, store_conditions='В любых условиях',
                                     shelf_life='12 месяцев', store_temperature='При -15',
                                     is_new=False, is_trendy=False)
    product2.categories.set(categories)
    product2.save()

    result = _make_list_to_assert_response(category_data=_categories_list[1], price=price, storage=storage,
                                           shelf_life=shelf_life, store_temperature=store_temperature,
                                           is_new=is_new, is_trendy=is_trendy)

    if data is not None:
        data = {'category_id': categories[data].id}

    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, data, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)
    assert response.status_code == status.HTTP_200_OK
    assert response.data == result


@pytest.mark.django_db
def test_not_found():
    """
    Ensure we can get product objects.
    """

    url = 'http://localhost:8060/filter/'

    request = RequestFactory().post(url, {'category_id': 9900}, content_type='application/json')
    news_view = FilterViewSet.as_view()
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == "Not Found"
