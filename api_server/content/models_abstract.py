from django.db import models
from django.utils.translation import ugettext_lazy as _

from api_server.models import TrackableSoftDeletableModel
from api_server.managers import SoftDeletableManager


class AbstractNews(TrackableSoftDeletableModel):
    objects = SoftDeletableManager()

    title = models.CharField(_('Title'), max_length=255)
    # image = models.ImageField()
    short_description = models.TextField(_('Short description'), max_length=2047)
    content = models.TextField(_('Content'))
    slug = models.SlugField(_('Slug'), max_length=255, unique=True, db_index=True)
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)
    is_active = models.BooleanField(_('Is active'), default=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
