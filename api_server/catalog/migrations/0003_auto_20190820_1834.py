# Generated by Django 2.1.7 on 2019-08-20 18:34

from django.db import migrations, models
import functools
import utils


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20190819_0956'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='icon',
            field=models.ImageField(default='', upload_to=functools.partial(utils._image_upload_to, *(), **{'dirname': 'category_icons'})),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.ImageField(default='', upload_to=functools.partial(utils._image_upload_to, *(), **{'dirname': 'category_images'})),
            preserve_default=False,
        ),
    ]
