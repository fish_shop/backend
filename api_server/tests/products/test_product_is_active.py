import pytest
from rest_framework import status
from rest_framework.test import APITestCase
from djmoney.money import Money

from catalog.factories import ProductFactory


@pytest.mark.django_db
class ProductTest(APITestCase):
    def test_get_all_products(self):
        """
        Ensure we can get product objects.
        """
        ProductFactory.create_batch(5, is_active=False, price=Money(10, 'RUB'), is_trendy=True,
                                    is_new=True)
        product2 = ProductFactory.create(price=Money(100, 'RUB'))

        url = 'http://0.0.0.0:8060/api/products/'
        request = self.client.get(url, {}, format='json')
        actual = request.data

        product = [
            {
                'id': product2.id,
                'name': product2.name,
                'slug': product2.slug,
                'categories': [],
                "images": [],
                'is_new': product2.is_new,
                'is_trendy': product2.is_trendy,
                'price': {
                    'amount': 100.00,
                    'currency': product2.price_currency,
                },
            },
        ]

        assert request.status_code == status.HTTP_200_OK
        assert actual == product
