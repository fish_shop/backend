import coreschema
from rest_framework.schemas import ManualSchema
import coreapi

chema_products = ManualSchema(fields=[
    coreapi.Field(
        "slug",
        required=False,
        location="path",
        schema=coreschema.String(),
        description='A unique string value identifying Product object',
    ),
    coreapi.Field(
        "query",
        required=False,
        location="query",
        schema=coreschema.String(),
        description='Text on name or description fields of products',
    ),
    coreapi.Field(
        "limit",
        required=False,
        location="query",
        schema=coreschema.Integer(),
        description='Number of results to return per page.',
    ),
    coreapi.Field(
        "offset",
        required=False,
        location="query",
        schema=coreschema.Integer(),
        description='The initial index from which to return the results.',
    ),
    coreapi.Field(
        "categories[]",
        required=False,
        location="query",
        schema=coreschema.Integer(),
        description='A unique integer value identifying Category object.',
    ),
    coreapi.Field(
        "store_temperature[]",
        required=False,
        location="query",
        schema=coreschema.String(),
        description='A string value identifying Product object in store_temperature field.',
    ),
    coreapi.Field(
        "shelf_life[]",
        required=False,
        location="query",
        schema=coreschema.String(),
        description='A string value identifying Product object in shelf_life field.',
    ),
    coreapi.Field(
        "store_conditions[]",
        required=False,
        location="query",
        schema=coreschema.String(),
        description='A string value identifying Product object in store_conditions field.',
    ),
    coreapi.Field(
        "min_price",
        required=False,
        location="query",
        schema=coreschema.Integer(),
        description='A integer value identifying Product object with price in range from min_price value.',
    ),
    coreapi.Field(
        "max_price",
        required=False,
        location="query",
        schema=coreschema.Integer(),
        description='A integer value identifying Product object with price in range to max_price value.',
    ),
    coreapi.Field(
        "is_new",
        required=False,
        location="query",
        schema=coreschema.Boolean(),
        description='A boolean value identifying Product object in is new product?',
    ),
    coreapi.Field(
        "is_trendy",
        required=False,
        location="query",
        schema=coreschema.Boolean(),
        description='A boolean value identifying Product object in is trendy product?',
    ),
    ],
    description="""
    Get Product objects
    
    
    Method: list:
    Get all Product objects
    Can filtering products
    
    Example:
        {
            'query': Black Shark,
            'categories[]': 1,
            'categories[]': 2,
            'categories[]': 3,
            'store_temperature[]': 'При температуре -10',
            'store_temperature[]': 'При температуре -30',
            'shelf_life[]': '12 месяцев',
            'shelf_life[]': '3 месяцев',
            'shelf_life[]': '24 месяцев',
            'shelf_life[]': '1 месяц',
            'store_conditions[]': 'На холоде',
            'store_conditions[]': 'В холодильнике',
            'store_conditions[]': 'В воде',
            'min_price': 1020,
            'is_new': True,
            'is_trendy': False,
        }

    
    Method: retrieve:
    Get full information about single Product object
    ---
    slug -- A unique string value identifying this Discount object.

    type:
        required: True,
        location: path,
    
    Example:
        slug: samsung
    """
)

filter_product_chema = ManualSchema(fields=[
    coreapi.Field(
        "category_id",
        required=False,
        location="form",
        schema=coreschema.Integer(),
        description='(int) Unique id of category object',
    ),
    coreapi.Field(
        "currency",
        required=False,
        location="form",
        schema=coreschema.String(),
        description='(string) Currency that you want get price'
    ),
], description="""Request to get block filters to filtering products for one category or all categories
    
    
    if category_id ----> filters for products that categories field have category category_id
    else ----> filters for all products
    
    # Parameters:
    ---

    type:
      category_id:
        required: false
        type: int
        example: 1
      currency:
        required: false
        type: string
        example: 'RUB'
        
    Example:
      {
        'category_id': 1,
        'currency': 'RUB'
      }
        """,
)
