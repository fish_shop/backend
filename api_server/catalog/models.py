from functools import partial

from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils import _image_upload_to
from .models_abstract import AbstractCategory, AbstractProduct, AbstractProductImage


class Category(AbstractCategory):
    icon = models.ImageField(upload_to=partial(_image_upload_to, dirname='category_icons'))
    image = models.ImageField(upload_to=partial(_image_upload_to, dirname='category_images'))

    is_header = models.BooleanField(_('Выводить в шапке'), default=False)
    is_popular = models.BooleanField(_('Популярная?'), default=False)

    class Meta:
        app_label = 'catalog'
        verbose_name_plural = "categories"
        ordering = ['-parent_category_id', 'sort_order', 'name']


class Product(AbstractProduct):
    is_new = models.BooleanField(_('Is new'), default=False)
    is_trendy = models.BooleanField(_('Is trendy'), default=False)
    related_products = models.ManyToManyField(
        'self', verbose_name=_('Related products'), null=True, blank=True,
        related_name='rel_products',
        related_query_name='rel_products'
    )
    store_conditions = models.CharField(
        _('Store conditions'),
        max_length=255,
        default='В любых условиях',
        blank=True,
        null=True,
    )
    shelf_life = models.CharField(
        _('Shelf life'),
        max_length=255,
        default='3 месяца',
    )
    store_temperature = models.CharField(
        _('Store temperature'),
        max_length=255,
        default='При температуре -10',
    )
    pack_weight = models.CharField(
        _('Package weight'),
        max_length=255,
        default='500 грамм',
    )

    categories = models.ManyToManyField(
        Category, verbose_name=_('Categories'),
        related_name='products',
        related_query_name='product'
    )

    class Meta:
        app_label = 'catalog'
        ordering = ['is_active', 'sort_order', 'name']


class ProductImage(AbstractProductImage):

    product = models.ForeignKey(Product, related_name='images', on_delete=models.CASCADE)
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)

    class Meta:
        ordering = ['sort_order']
        app_label = 'catalog'
