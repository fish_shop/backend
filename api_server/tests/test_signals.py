from typing import List

import pytest
from rest_framework.test import APITestCase

from api_server.settings import MAX_CATEGORIES_DEPTH
from catalog.factories import CategoryFactory, ProductFactory
from catalog.models import Category

_data_check_parent_loops__RecursionError = [2, 8]


def _create_linked_categories_list(length: int) -> List[Category]:
    categories = []

    for i in range(length):
        cat = CategoryFactory.create(name=f'Category {i}')
        # cat = CategoryFactory.create(name_ru=f'Category {i}')
        if categories:
            cat.parent_category = categories[-1]
        cat.save()
        categories.append(cat)

    return categories


@pytest.mark.django_db
@pytest.mark.parametrize('loop_length', _data_check_parent_loops__RecursionError)
def test_check_parent_loops__RecursionError(loop_length: int):
    categories = _create_linked_categories_list(loop_length)
    categories[0].parent_category = categories[-1]
    with pytest.raises(RecursionError):
        categories[0].save()


@pytest.mark.django_db
class SignalTest(APITestCase):

    @staticmethod
    def test_check_parent_eq_self__RecursionError():
        category = CategoryFactory.create(name='Test')
        # category = CategoryFactory.create(name_ru='Test')

        category.parent_category = category
        with pytest.raises(RecursionError):
            category.save()

    @staticmethod
    def test_check_nested_categories_depth__RecursionError():
        parent_category = CategoryFactory.create()

        for _ in range(MAX_CATEGORIES_DEPTH):
            cat = CategoryFactory.create(parent_category=parent_category)
            parent_category = cat

        last_cat = CategoryFactory.build(parent_category=parent_category)
        with pytest.raises(RecursionError):
            last_cat.save()

    @pytest.mark.skip
    @staticmethod
    def test_products_categories_assign_by_parent_id():
        # TODO: придумать решение. Сейчас не работает из-за транзакции
        categories = _create_linked_categories_list(3)

        another_cat = CategoryFactory.create()

        product = ProductFactory.create()
        product.categories.add(categories[-1])
        product.save()

        product_categories_ids = set(cat.id for cat in product.categories.all())
        assert product_categories_ids == set(cat.id for cat in categories)
        assert another_cat.id not in product_categories_ids
