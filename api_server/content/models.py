from functools import partial

from django.utils.translation import ugettext_lazy as _
from django.db import models
from api_server.models import TrackableSoftDeletableModel
from api_server.managers import SoftDeletableManager

from catalog.models import Product
from content.models_abstract import AbstractNews
from utils import _image_upload_to


class News(AbstractNews):
    image = models.ImageField(upload_to=partial(_image_upload_to, dirname='news'), blank=True, null=True)

    class Meta:
        app_label = 'content'
        verbose_name_plural = "News"
        ordering = ['sort_order', 'created_at', 'title']


class Recipe(AbstractNews):
    image = models.ImageField(upload_to=partial(_image_upload_to, dirname='recipes'), blank=True, null=True)
    products = models.ManyToManyField(
        Product, verbose_name=_('Products'),
        related_name='recipes',
        related_query_name='recipe'
    )

    class Meta:
        app_label = 'content'
        ordering = ['sort_order', 'created_at', 'title']


class Discount(AbstractNews):
    image = models.ImageField(upload_to=partial(_image_upload_to, dirname='discounts'), blank=True, null=True)

    class Meta:
        app_label = 'content'
        ordering = ['sort_order', 'created_at', 'title']


class TradePlace(TrackableSoftDeletableModel):
    objects = SoftDeletableManager()

    title = models.CharField(_('Title'), max_length=255)
    address = models.TextField(_('Address'))
    opening_hours = models.TextField(_('Opening Hours'))
    longitude = models.DecimalField(_('Longitude coordinate'), max_digits=9, decimal_places=6)
    latitude = models.DecimalField(_('Latitude coordinate'), max_digits=9, decimal_places=6)
    is_active = models.BooleanField(_('Is active'), default=True)
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)

    # slug = models.SlugField(_('Slug'), max_length=255, unique=True, db_index=True)

    def __str__(self):
        return self.title

    class Meta:
        app_label = 'content'
        ordering = ['title', 'sort_order']
