import pytest
from django.test import RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase

from content.api.views import TradePlaceViewSet
from content.factories import TradePlaceFactory


@pytest.mark.django_db
class TradePlaceTest(APITestCase):
    @staticmethod
    def test_get_all_news():
        shop_list = []

        shop_list.append(TradePlaceFactory.create(title='Test1', sort_order=1, longitude=0.323565,
                                                  latitude=12.434401))
        shop_list.append(TradePlaceFactory.create(title='Test2', sort_order=10, longitude=1.545821,
                                                  latitude=17.434001))
        shop_list.append(TradePlaceFactory.create(title='Test3', sort_order=100, longitude=92.010311,
                                                  latitude=56.011231))

        request = RequestFactory().get(f'api/shops/')
        shop_view = TradePlaceViewSet.as_view({'get': 'list'})
        response = shop_view(request)

        actual = response.data
        expected = [
            {
                'title': shop.title,
                'address': shop.address,
                'opening_hours': shop.opening_hours,
                'longitude': str(shop.longitude),
                "latitude":  str(shop.latitude),
            }
            for shop in shop_list
        ]
        print(actual)
        print(expected)
        assert response.status_code == status.HTTP_200_OK
        assert actual == expected
