from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from .models import Category, Product, ProductImage


class TranslationWidgetsAdmin(TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }


class CategoryAdmin(TranslationWidgetsAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        # 'slug',
        'parent_category',
        'sort_order',
        'is_active',
    )
    list_filter = ('is_active', 'parent_category')
    list_editable = ('sort_order', 'is_active')


class ProductImageInline(admin.TabularInline):
    fields = ('image', 'sort_order', 'is_active')
    model = ProductImage


class ProductAdmin(TranslationWidgetsAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = (
        'name',
        # 'slug',
        'price',
        'sort_order',
        'is_active',
    )
    list_editable = ('sort_order', 'is_active')
    list_filter = ('is_active', 'categories')
    inlines = [
        ProductImageInline,
    ]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
