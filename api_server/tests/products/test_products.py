import pytest
from django.utils.translation import activate, deactivate
from django.test import RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase
from djmoney.money import Money

from catalog.api.views import ProductViewSet
from catalog.factories import ProductFactory


@pytest.mark.django_db
def test_get_all_products():
    """
    Ensure we can get product objects.
    """
    product1 = ProductFactory.create(price=Money(10, 'RUB'), name='Test1', sort_order=100,
                                     is_trendy=True, is_new=True)
    product2 = ProductFactory.create(price=Money(0, 'RUB'), name='Test2', sort_order=1000,
                                     is_trendy=False, is_new=False)

    # product1 = ProductFactory.create(price=Money(10, 'RUB'), name_ru='Test1', name_en='Test1', sort_order=100,
    #                                  is_trendy=True, is_new=True)
    # product2 = ProductFactory.create(price=Money(0, 'RUB'), name_ru='Test2', name_en='Test2', sort_order=1000,
    #                                  is_trendy=False, is_new=False)

    product = [
        {
            'id': product1.id,
            'name': product1.name,
            'slug': product1.slug,
            'categories': [],
            "images": [],
            'is_new': product1.is_new,
            'is_trendy': product1.is_trendy,
            'price': {
                'amount': product1.price.amount,
                'currency': product1.price_currency,
            },
        },
        {
            'id': product2.id,
            'name': product2.name,
            'slug': product2.slug,
            'categories': [],
            "images": [],
            'is_new': product2.is_new,
            'is_trendy': product2.is_trendy,
            'price': {
                'amount': product2.price.amount,
                'currency': product2.price_currency,
            },
        },
    ]

    url = 'http://0.0.0.0:8060/api/products/'

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    actual = response.data

    assert response.status_code == status.HTTP_200_OK
    assert actual == product


# - Список товаров
# GET `/api/products/`
@pytest.mark.django_db
class ProductTest(APITestCase):

    def test_get_one_product(self):
        """
        Ensure we can get product objects.
        """
        product = ProductFactory.create(price=Money(5, 'RUB'), is_trendy=True, is_new=False)

        url = f'http://0.0.0.0:8060/api/products/{product.slug}/'
        request = self.client.get(url, {}, format='json')
        actual = request.data

        product = {
            'id': product.id,
            'name': product.name,
            'slug': product.slug,
            'categories': [],
            'is_new': False,
            'is_trendy': True,
            'price': {
                'amount': 5.00,
                'currency': 'RUB',
            },
            "images": [],
            'description': product.description,
            'store_conditions': product.store_conditions,
            'shelf_life': product.shelf_life,
            'store_temperature': product.store_temperature,
            'pack_weight': product.pack_weight,
            'recipes': [],
            'related_products': [],
        }

        assert request.status_code == status.HTTP_200_OK
        assert product == actual

    def test_product_multilang_fields(self):
        """
        1. Создает категорию с мультиязычными полями на всех языках
        2. Для каждого языка устанавлиевает его в локаль и отправляет запрос
        3. Проверяет, что мультиязычные поля отдаются на правильном языке в зав-ти от локали
        """

        product = ProductFactory.create()

        activate('ru')
        url = f'http://0.0.0.0:8060/api/products/{product.slug}/'
        request = self.client.get(url, {}, format='json')
        actual = request.data

        deactivate()

        assert actual['name'] == product.name
        assert actual['description'] == product.description

        # assert actual['name'] == product.name_ru
        # assert actual['description'] == product.description_ru

        activate('en')

        request = self.client.get(url, {}, format='json')
        actual = request.data

        deactivate()

        assert actual['name'] == product.name
        assert actual['description'] == product.description

        # assert actual['name'] == product.name_en
        # assert actual['description'] == product.description_en
