import pytest
from django.utils.translation import activate, deactivate
from django.test import RequestFactory
from rest_framework import status
from djmoney.money import Money

from catalog.api.views import ProductViewSet
from catalog.factories import ProductFactory, CategoryFactory


def write_to_url_and_return(url: str, key, value):
    if type(value) is list:
        for val in value:
            url = url + '&{0}={1}'.format(key, val)
    else:
        url = url + '&{0}={1}'.format(key, value)
    return url


@pytest.mark.django_db
@pytest.mark.parametrize('count, res_count, query, limit, offset, next_link, previous_link, category, min_price, '
                         'max_price, store, shelf_life, store_temperature, is_new, is_trendy', [
                             (
                                     10, 10, "Samsung", 20, 0, False, False, [0, 1, 2, 3, 4], 10, 900,
                                     ['В холодильнике', 'У девушки дома'],
                                     ['12 месяцев', '6 месяцев'], ['12', '6'], False, True
                             ),
                             (
                                     10, 5, "Samsung", 5, 0, True, False, [0, 1, 2, 3, 4], 10, 900,
                                     ['В холодильнике', 'У девушки дома'],
                                     ['12 месяцев', '6 месяцев'], ['12', '6'], False, True
                             ),
                             (
                                     10, 5, "Samsung", 5, 3, True, True, [0, 1, 2, 3, 4], 10, 900,
                                     ['В холодильнике', 'У девушки дома'],
                                     ['12 месяцев', '6 месяцев'], ['12', '6'], False, True
                             ),
                             (
                                     10, 0, "Samsung", 5, 10, False, True, [0, 1, 2, 3, 4], 10, 900,
                                     ['В холодильнике', 'У девушки дома'],
                                     ['12 месяцев', '6 месяцев'], ['12', '6'], False, True
                             ),
                             (
                                     10, 10, "Notebook Huawei", 50, 0, False, False, [0, 1, 2, 3, 4], 10, 5900,
                                     ['На кухне', 'У девушки дома'],
                                     ['12 месяцев', '1 месяц'], ['12', '1'], True, True
                             ),
                             (
                                     10, 2, "Notebook Huawei", 5, 8, False, True, [0, 1, 2, 3, 4], 10, 5900,
                                     ['На кухне', 'У девушки дома'],
                                     ['12 месяцев', '1 месяц'], ['12', '1'], True, True
                             ),
                             (
                                     50, 25, "", 25, 8, True, True, [0, 1, 2, 3, 4], 10, 5900,
                                     ['У девушки дома', 'В холодильнике', 'В морозе', 'В воде', 'На кухне'],
                                     ['12 месяцев', '6 месяцев', '9 месяцев', '3 месяцев', '1 месяц'],
                                     ['12', '6', '9', '3', '1'], None, None
                             ),
                             (
                                     50, 42, "", 50, 8, False, True, [0, 1, 2, 3, 4], 10, 5900,
                                     ['У девушки дома', 'В холодильнике', 'В морозе', 'В воде', 'На кухне'],
                                     ['12 месяцев', '6 месяцев', '9 месяцев', '3 месяцев', '1 месяц'],
                                     ['12', '6', '9', '3', '1'], None, None
                             ),
                             (
                                     30, 22, "", 50, 8, False, True, [0, 1, 2, 3, 4], 100, 3900,
                                     ['У девушки дома', 'В холодильнике', 'В морозе', 'В воде', 'На кухне'],
                                     ['12 месяцев', '6 месяцев', '9 месяцев', '3 месяцев', '1 месяц'],
                                     ['12', '6', '9', '3', '1'], None, None
                             ),
                             (
                                     10, 2, "", 50, 8, False, True, [0, 1], 100, 3900,
                                     ['У девушки дома', 'В холодильнике', 'В морозе', 'В воде', 'На кухне'],
                                     ['12 месяцев', '6 месяцев', '9 месяцев', '3 месяцев', '1 месяц'],
                                     ['12', '6', '9', '3', '1'], None, None
                             ),
                             (
                                     0, 0, "a", 10, None, False, False, [0, 1], 110, 100,
                                     ['У девушки дома', 'В холодильнике', 'В воде'],
                                     ['12 месяцев', '6 месяцев', '3 месяцев'], ['12', '6', '3'],  False, True
                             ),
                         ])
def test_product_filter_search_pagination(count, res_count, query, limit, offset, next_link, previous_link,
                                          category, min_price, max_price, store, shelf_life, store_temperature,
                                          is_new, is_trendy):
    activate('ru')

    category_list = create_list()
    response_dict = {}

    # -----------------------------------
    # -- Создание словаря для url
    # -----------------------------------

    if query is not None:
        response_dict["query"] = query

    # -------------------------
    # -- Категории
    # -------------------------
    if category is not None:
        response_dict["categories[]"] = list()
        for index in category:
            response_dict["categories[]"].append(category_list[index])

    # -------------------------
    # -- Цена
    # -------------------------

    if min_price is not None:
        response_dict["min_price"] = min_price
    if max_price is not None:
        response_dict["max_price"] = max_price

    # -------------------------
    # -- Checkbox
    # -------------------------
    if store is not None:
        response_dict["store_conditions[]"] = store
    if shelf_life is not None:
        response_dict["shelf_life[]"] = shelf_life
    if store_temperature is not None:
        response_dict["store_temperature[]"] = store_temperature

    # --------------------------
    # -- Boolean
    # --------------------------

    if is_new is not None:
        response_dict["is_new"] = is_new
    if is_trendy is not None:
        response_dict["is_trendy"] = is_trendy

    # --------------------------
    # -- limit offset
    # --------------------------

    if limit is not None:
        response_dict["limit"] = limit
    if offset is not None:
        response_dict["offset"] = offset

    url = 'http://0.0.0.0:8060/api/products/?'

    # -----------------------------------
    # -- Создание url из словаря
    # -----------------------------------

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    # -----------------------------
    # -- Получаем данные
    # -----------------------------

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)
    actual = response.data

    assert response.status_code == status.HTTP_200_OK
    assert actual["count"] == count

    if next_link is True:
        assert actual["next"] is not None
    else:
        assert actual["next"] is None

    if previous_link is True:
        assert actual["previous"] is not None
    else:
        assert actual["previous"] is None

    assert len(actual["results"]) == res_count

    deactivate()


@pytest.mark.django_db
def create_list():
    # ------------------------------
    # -- Создание категорий
    # ------------------------------

    category_list = []
    for num in range(5):
        category = CategoryFactory.create(name=f'Test{num}', sort_order=num)
        # category = CategoryFactory.create(name_ru=f'Test{num}', name_en=f'Test{num}', sort_order=num)
        category_list.append(category.id)

    # ------------------------------
    # -- Создание продуктов
    # ------------------------------

    for _ in range(10):
        product = ProductFactory.create(name='Samsung', description='Asus and Phone', price=Money(10, 'RUB'),
                                        is_trendy=True, is_new=False, store_conditions='У девушки дома',
                                        shelf_life='12 месяцев', store_temperature='12')
        product.categories.set([category_list[0]])
        product.save()

        product = ProductFactory.create(name='Apple', description='Notebook and Samsung', price=Money(100, 'RUB'),
                                        is_trendy=False, is_new=False, store_conditions='В холодильнике',
                                        shelf_life='6 месяцев', store_temperature='6')
        product.categories.set([category_list[1]])
        product.save()

        product = ProductFactory.create(name='Nokia', description='Phone Apple', price=Money(500, 'RUB'),
                                        is_trendy=True, is_new=True, shelf_life='9 месяцев',
                                        store_conditions='В морозе', store_temperature='9')
        product.categories.set([category_list[2]])
        product.save()

        product = ProductFactory.create(name='Huawei', description='Notepad and Nokia', price=Money(2000, 'RUB'),
                                        is_trendy=False, is_new=True,
                                        store_conditions='В воде', shelf_life='3 месяцев', store_temperature='3')
        product.categories.set([category_list[3]])
        product.save()

        product = ProductFactory.create(name='Asus', description='Notebook and Huawei', price=Money(5000, 'RUB'),
                                        is_trendy=True, is_new=True,
                                        store_conditions='На кухне', shelf_life='1 месяц', store_temperature='1')
        product.categories.set([category_list[4]])
        product.save()

    # for _ in range(10):
    #     product = ProductFactory.create(name_ru='Samsung', description_ru='Asus and Phone', price=Money(10, 'RUB'),
    #                                     is_trendy=True, is_new=False, store_conditions='У девушки дома',
    #                                     shelf_life='12 месяцев', store_temperature='12')
    #     product.categories.set([category_list[0]])
    #     product.save()
    #
    #     product = ProductFactory.create(name_ru='Apple', description_ru='Notebook and Samsung', price=Money(100, 'RUB'),
    #                                     is_trendy=False, is_new=False, store_conditions='В холодильнике',
    #                                     shelf_life='6 месяцев', store_temperature='6')
    #     product.categories.set([category_list[1]])
    #     product.save()
    #
    #     product = ProductFactory.create(name_ru='Nokia', description_ru='Phone Apple', price=Money(500, 'RUB'),
    #                                     is_trendy=True, is_new=True, shelf_life='9 месяцев',
    #                                     store_conditions='В морозе', store_temperature='9')
    #     product.categories.set([category_list[2]])
    #     product.save()
    #
    #     product = ProductFactory.create(name_ru='Huawei', description_ru='Notepad and Nokia', price=Money(2000, 'RUB'),
    #                                     is_trendy=False, is_new=True,
    #                                     store_conditions='В воде', shelf_life='3 месяцев', store_temperature='3')
    #     product.categories.set([category_list[3]])
    #     product.save()
    #
    #     product = ProductFactory.create(name_ru='Asus', description_ru='Notebook and Huawei', price=Money(5000, 'RUB'),
    #                                     is_trendy=True, is_new=True,
    #                                     store_conditions='На кухне', shelf_life='1 месяц', store_temperature='1')
    #     product.categories.set([category_list[4]])
    #     product.save()

    return category_list
