from functools import partial

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField

from api_server.managers import SoftDeletableManager
from api_server.models import TrackableSoftDeletableModel, SoftDeletableModel
from utils import _image_upload_to


class AbstractCategory(TrackableSoftDeletableModel):
    objects = SoftDeletableManager()

    name = models.CharField(_('Name'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, unique=True, db_index=True)
    parent_category = models.ForeignKey('self', null=True, blank=True,
                                        verbose_name=_('Parent category'), on_delete=models.CASCADE)
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)
    is_active = models.BooleanField(_('Is active'), default=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class AbstractProduct(TrackableSoftDeletableModel):
    objects = SoftDeletableManager()

    name = models.CharField(_('Name'), max_length=255)
    slug = models.SlugField(_('Slug'), max_length=255, unique=True, db_index=True)
    # categories = models.ManyToManyField(Category, verbose_name=_('Categories'),
    #                                     related_name='products',
    #                                     related_query_name='product')
    description = models.TextField(_('Description'), max_length=2047)
    price = MoneyField(
        _('Price'),
        max_digits=10,
        decimal_places=2,
        default_currency='RUB',
        blank=True,
        null=True,
        default=0.0,
    )
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)
    is_active = models.BooleanField(_('Is active'), default=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class AbstractProductImage(SoftDeletableModel):
    image = models.ImageField(upload_to=partial(_image_upload_to, dirname='products'))
    # product = models.ForeignKey(Product, related_name='_product_images', on_delete=models.CASCADE)
    is_active = models.BooleanField(_('Is active'), default=True)
    sort_order = models.PositiveIntegerField(_('Sort order'), default=100)

    class Meta:
        abstract = True


# TODO: нужно добавить отзывы к товарам
class AbstractReview(TrackableSoftDeletableModel):
    objects = SoftDeletableManager()

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_('User'), related_name='comment')
    title = models.CharField(_('Title'), max_length=255)
    content = models.TextField(_('Content'))
    # product = models.ForeignKey(Product, related_name='_product_reviews', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
