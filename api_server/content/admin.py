from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from content.models import News, Recipe, Discount, TradePlace


class TranslationWidgetsAdmin(TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }


class AbstractContentAdmin:
    exclude = ('is_removed', 'removed_at')
    list_display = ('title', 'short_description', 'slug', 'sort_order', 'is_active')
    list_filter = ('is_active',)
    list_editable = ('is_active', 'sort_order')


class NewsAdminAbstract(AbstractContentAdmin, TranslationWidgetsAdmin):
    pass


class RecipeAdminAbstract(AbstractContentAdmin, TranslationWidgetsAdmin):
    pass


class DiscountAdminAbstract(AbstractContentAdmin, TranslationWidgetsAdmin):
    pass


class ShopAdminAbstract(admin.ModelAdmin):
    exclude = ('is_removed', 'removed_at')
    list_display = ('title', 'address', 'opening_hours', 'longitude', 'latitude', 'sort_order', 'is_active')
    list_filter = ('is_active', 'opening_hours')
    list_editable = ('is_active', 'sort_order')


admin.site.register(News, NewsAdminAbstract)
admin.site.register(Recipe, RecipeAdminAbstract)
admin.site.register(Discount, DiscountAdminAbstract)
admin.site.register(TradePlace, ShopAdminAbstract)
