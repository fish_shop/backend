# fish-shop django back-end

## Contributing
#### Installation
1. (_Pycharm only_) Mark `api_server` & `thumbnailer` dirs as *Sources roots*.
2. Activate virtualenv `python3 -m venv venv` and `source venv/bin/activate`
3. `pip3 install -r requirements.txt`
4. Запускаем БД
5. `make migrate`
6. Создаем пользователя для админки: `make createsuperuser`


## Run without Docker (Запуск без докера)
1. (_Pycharm only_) Mark `api_server` & `thumbnailer` dirs as *Sources roots*.
2. Activate virtualenv `python3 -m venv venv` and `source venv/bin/activate`
3. `pip3 install -r requirements.txt`
4. Install Database PostgreSQL 
`sudo apt-get install postgresql postgresql-client postgresql-common`
5. Run Database shell `sudo -u postgres psql`
6. Create Database `CREATE DATABASE mydjangoproject_db;`
7. Create user for database `CREATE USER "djangoUser" WITH PASSWORD 'rootuser';`
8. Give to user privileges to database `GRANT ALL PRIVILEGES ON DATABASE  mydjangoproject_db TO "djangoUser";`
9. Close database shell `\q`
10. Open file `back-end/api_server/api_server/settings.py`
11. Find field `DATABASES = { ... }` and write params of creating database
12. In field `USER` write `'USER': 'username_of_your_database',`
13. In field `PASSWORD` write `'PASSWORD': 'password_of_user',`
14. In field `HOST` write `'HOST': 'localhost',`
15. In field `NAME` write `'NAME': 'имя_созданной_БД'` 
16. For example:
#####  DATABASES = {
       'default': {
       'ENGINE': 'django.db.backends.postgresql',
       'HOST': 'localhost',
       'USER': 'MyUser',
       'PASSWORD': 'mypassword',
       'NAME': 'my_User_db'},
##### }
17. `make migrate`
18. Создаем пользователя для админки: `make createsuperuser`
19. `make makemigrations`
20. `make migrate`
21. `make run-server`