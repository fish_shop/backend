import pytest
from django.utils.translation import activate, deactivate
from django.test import RequestFactory
from rest_framework import status
from djmoney.money import Money

from catalog.api.views import ProductViewSet
from catalog.factories import ProductFactory, CategoryFactory


def write_to_url_and_return(url: str, key, value):
    if type(value) is list:
        for val in value:
            url = url + '&{0}={1}'.format(key, val)
    else:
        url = url + '&{0}={1}'.format(key, value)
    return url


def create_categories_products_return_category_list():
    category1 = CategoryFactory.create(name='Test1', sort_order=100)
    category2 = CategoryFactory.create(name='Test2', sort_order=1000)

    # category1 = CategoryFactory.create(name_ru='Test1', name_en='Test1', sort_order=100)
    # category2 = CategoryFactory.create(name_ru='Test2', name_en='Test2', sort_order=1000)

    product = ProductFactory.create(
        price=Money(5000, 'RUB'), is_trendy=True, is_new=True,
        store_conditions='У девушки дома', shelf_life='12 месяцев', store_temperature='12'
        # store_conditions='У девушки дома', country='США'
    )
    product.categories.set([category1])
    product.save()

    product = ProductFactory.create(
        price=Money(100, 'RUB'), is_trendy=False, is_new=True,
        store_conditions='В холодильнике', shelf_life='6 месяцев', store_temperature='6'
        # store_conditions='В холодильнике', country='Китай'
    )
    product.categories.set([category2])
    product.save()

    product = ProductFactory.create(
        price=Money(500, 'RUB'), is_trendy=True, is_new=False,
        store_conditions='В холодильнике', shelf_life='12 месяцев', store_temperature='12'
        # store_conditions='В холодильнике', country='США'
    )
    product.categories.set([category1])
    product.save()

    product = ProductFactory.create(
        price=Money(100, 'RUB'), is_trendy=False, is_new=False,
        store_conditions='В воде', shelf_life='9 месяцев', store_temperature='9'
        # store_conditions='В воде', country='Индия'
    )
    product.categories.set([category2])
    product.save()

    product = ProductFactory.create(
        price=Money(5000, 'RUB'), is_trendy=True, is_new=True,
        store_conditions='У девушки дома', shelf_life='6 месяцев', store_temperature='6'
        # store_conditions='У девушки дома', country='Китай'
    )
    product.categories.set([category1])
    product.save()

    return [category1, category2]


@pytest.mark.django_db
@pytest.mark.parametrize('count, category', [
    (3, [0]),
    (2, [1]),
    (5, [0, 1])
])
def test_filter_category(count, category):
    activate('ru')

    category_list = create_categories_products_return_category_list()

    response_dict = dict()
    response_dict["categories[]"] = []

    for index in category:
        response_dict["categories[]"].append(category_list[index].id)

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize('count, min_price, max_price', [
    (5, 50, None),
    (5, 100, None),
    (3, 500, None),
    (2, 1000, None),
    (2, 5000, None),
    (0, 50000, None),
    (5, None, 50000),
    (5, None, 5000),
    (3, None, 1000),
    (3, None, 500),
    (2, None, 100),
    (0, None, 50),
    (0, 100, 50),
    (2, 100, 400),
    (3, 100, 500),
    (1, 150, 500),
    (3, 150, 5090),
    (3, 500, 5090),
    (2, 560, 5090),
])
def test_filter_price(count, min_price, max_price):
    activate('ru')

    create_categories_products_return_category_list()

    response_dict = {}

    if min_price is not None:
        response_dict["min_price"] = min_price

    if max_price is not None:
        response_dict["max_price"] = max_price

    url = 'http://0.0.0.0:8060/api/products/?'
    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize('count, is_trendy', [
    (3, True),
    (2, False),
    (5, None),
])
def test_filter_is_trend(count, is_trendy):
    activate('ru')

    create_categories_products_return_category_list()

    response_dict = {}

    if is_trendy is not None:
        response_dict["is_trendy"] = is_trendy

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize('count, is_new', [
    (3, True),
    (2, False),
])
def test_filter_is_new(count, is_new):
    activate('ru')

    create_categories_products_return_category_list()

    response_dict = {}

    if is_new is not None:
        response_dict["is_trendy"] = is_new

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize('count, store_conditions', [
    (2, 'У девушки дома'),
    (2, 'В холодильнике'),
    (3, ['В холодильнике', 'В воде']),
    (3, ['В воде', 'У девушки дома']),
    (4, ['В холодильнике', 'У девушки дома']),
    (5, ['В холодильнике', 'У девушки дома', 'В воде']),
])
def test_filter_storage_conditions(count, store_conditions):
    activate('ru')

    create_categories_products_return_category_list()

    response_dict = {}

    if store_conditions is not None:
        response_dict["store_conditions[]"] = store_conditions

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize('count, shelf_life', [
    (2, '12 месяцев'),
    (2, '6 месяцев'),
    (1, '9 месяцев'),
    (3, ['9 месяцев', '6 месяцев']),
    (4, ['12 месяцев', '6 месяцев']),
    (3, ['9 месяцев', '12 месяцев']),
    (5, ['9 месяцев', '12 месяцев', '6 месяцев']),
])
def test_filter_shelf_life(count, shelf_life):
    activate('ru')

    create_categories_products_return_category_list()

    response_dict = {}

    if shelf_life is not None:
        response_dict["shelf_life[]"] = shelf_life

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize('count, store_temperature', [
    (2, '12'),
    (2, '6'),
    (1, '9'),
    (3, ['9', '6']),
    (4, ['12', '6']),
    (3, ['9', '12']),
    (5, ['9', '12', '6']),
])
def test_filter_store_temperature(count, store_temperature):
    activate('ru')

    create_categories_products_return_category_list()

    response_dict = {}

    if store_temperature is not None:
        response_dict["store_temperature[]"] = store_temperature

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()


@pytest.mark.django_db
@pytest.mark.parametrize(
    'count, category, min_price, max_price, store, shelf_life, store_temperature, is_new, is_trend', [
        (1, [1], 100, 900, 'В воде', ['9 месяцев'], ['9'], False, False),
        (1, [0, 1], 90, 900, 'В холодильнике', ['9 месяцев', '12 месяцев'], ['9', '12'], False, True),
        (2, [0], 5000, 5000, 'У девушки дома', ['6 месяцев', '12 месяцев'], ['6', '12'], True, True),
        (1, [0], 500, 501, ['В холодильнике', 'У девушки дома'], ['9 месяцев', '12 месяцев', '6 месяцев'],
         ['9', '12', '6'], False, True),
        (1, [0, 1], 100, 900, ['В холодильнике', 'В воде'], ['12 месяцев', '6 месяцев'], ['12', '6'], False, True),
        (0, [0, 1], 100, 9000, ['В воде', 'У девушки дома'], ['12 месяцев'], ['12'], False, True),
        (1, [0, 1], 100, 5000, ['В воде', 'У девушки дома'], ['12 месяцев'], ['12'], True, True),
        (0, [0, 1], 110, 100, ['В воде', 'У девушки дома', 'В холодильнике'], ['9 месяцев', '6 месяцев', '12 месяцев'],
         ['9', '6', '12'], False, True),
    ])
def test_filter_all_params(count, category, min_price, max_price, store, shelf_life, store_temperature, is_new,
                           is_trend):
    activate('ru')

    category_list = create_categories_products_return_category_list()

    response_dict = dict()
    response_dict["categories[]"] = []

    for index in category:
        response_dict["categories[]"].append(category_list[index].id)

    if min_price is not None:
        response_dict["min_price"] = min_price
    if max_price is not None:
        response_dict["max_price"] = max_price

    if store is not None:
        response_dict["store_conditions[]"] = store
    if shelf_life is not None:
        response_dict["shelf_life[]"] = shelf_life
    if store_temperature is not None:
        response_dict["store_temperature[]"] = store_temperature

    if is_new is not None:
        response_dict["is_new"] = is_new
    if is_trend is not None:
        response_dict["is_trendy"] = is_trend

    url = 'http://0.0.0.0:8060/api/products/?'

    for key, value in response_dict.items():
        url = write_to_url_and_return(url, key, value)

    request = RequestFactory().get(url)
    news_view = ProductViewSet.as_view({'get': 'list'})
    response = news_view(request)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == count

    deactivate()
