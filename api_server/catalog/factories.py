import factory
from factory.django import DjangoModelFactory

from catalog.models import Category, Product


class CategoryFactory(DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.Faker('name', locale='ru_RU')

    # translation fields

    # name_ru = factory.Faker('name', locale='ru_RU')
    # name_en = factory.Faker('name', locale="en_US")

    slug = factory.Faker('uuid4', cast_to=str)
    sort_order = factory.Faker('pyint', locale='ru_RU', min_value=100, max_value=9999, step=1)

    is_header = factory.Faker('pybool', locale="ru_RU")
    is_popular = factory.Faker('pybool', locale="ru_RU")


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = Product

    price_currency = 'RUB'
    price = factory.Faker('pyint', min_value=0, max_value=10000000, step=1)

    name = factory.Faker('word', locale="ru_RU", ext_word_list=None)

    # name_ru = factory.Faker('word', locale="ru_RU", ext_word_list=None)
    # name_en = factory.Faker('word', locale="en_US", ext_word_list=None)

    description = factory.Faker('text', locale="ru_RU", max_nb_chars=2000, ext_word_list=None)

    # description_ru = factory.Faker('text', locale="ru_RU", max_nb_chars=2000, ext_word_list=None)
    # description_en = factory.Faker('text', locale="en_US", max_nb_chars=2000, ext_word_list=None)

    is_new = factory.Faker('pybool', locale="ru_RU")
    is_trendy = factory.Faker('pybool', locale="ru_RU")

    store_conditions = factory.Faker('job', locale="ru_RU")
    shelf_life = factory.Faker('text', locale="ru_RU", max_nb_chars=20, ext_word_list=None)
    store_temperature = factory.Faker('text', locale="ru_RU", max_nb_chars=30, ext_word_list=None)
    pack_weight = factory.Faker('text', locale="ru_RU", max_nb_chars=25, ext_word_list=None)

    slug = factory.Faker('uuid4', cast_to=str)
    sort_order = factory.Faker('pyint', locale="ru_RU", min_value=100, max_value=9999, step=1)
