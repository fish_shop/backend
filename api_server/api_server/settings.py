"""
Django settings for api_server project.

Generated by 'django-admin startproject' using Django 2.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os

from django.utils.translation import ugettext_lazy as _


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'c0xikpqb(9+9d9*)1dnkabe(@x&05@$z%01tjt7jyz088v0e6p'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    '0.0.0.0',
    'localhost',
    '127.0.0.1',
    'dev.seafoods-group.ru',
    'seafoods-group.ru',
]

CURRENCIES = ('RUB')
CURRENCY_CHOICES = [('RUB', 'RUB')]

# Application definition

INSTALLED_APPS = [
    # Translation. !! Its important to place 'modeltranslation' before 'django.contrib.admin'
    'modeltranslation',

    # Django Jet Admin
    'jet',
    'jet.dashboard',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djmoney.contrib.exchange',
    'django_filters',
    'corsheaders',
    'rest_framework_swagger',

    # 3rd party apps
    'rest_framework',
    'djmoney',

    # My own apps
    'catalog',
    'content',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',

    # CORS
    'corsheaders.middleware.CorsMiddleware',

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DATETIME_FORMAT': "%d.%m.%Y",
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),

    # Parser classes to help swagger, default ll be JSONParser only.
    # Parser classes priority-wise for Swagger

    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
        'rest_framework.parsers.JSONParser',
    ],
}

ROOT_URLCONF = 'api_server.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'api_server.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'USER': os.getenv('POSTGRES_USER', 'waycity_user'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', '123456'),
        'HOST': os.getenv('POSTGRES_HOST', '127.0.0.1'),
        'PORT': int(os.getenv('POSTGRES_PORT', '35432')),
        'NAME': os.getenv('POSTGRES_NAME', 'waycity_db')
    },
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_ROOT = os.getenv('STATIC_ROOT', os.path.join(BASE_DIR, 'static'))
STATIC_URL = '/django_static/'

MEDIA_ROOT = os.getenv('MEDIA_ROOT', os.path.join(BASE_DIR, 'media'))
MEDIA_URL = '/django_media/'



LANGUAGES = (
    ('ru', _('Russian')),
    ('en', _('English')),
)


CORS_ORIGIN_WHITELIST = [
    'localhost:3000',
    '127.0.0.1:3000',
    'app.jetadmin.io',
    'http://app.jetadmin.io',
    'https://app.jetadmin.io',
]

# Thumbnailer settings for Django apps

# THUMBNAILER_PREPEND = '/thumbnailer'
#
# THUMBNAILER_DEFAULT_MEDIA = 'web'
# THUMBNAILER_DEFAULT_VARIANT = ''
# THUMBNAILER_DEFAULT_MODIFIER = ''
#
# THUMBNAIL_PROPERTY_NAME = 'thumbnail'


# Catalog

MAX_CATEGORIES_DEPTH = 10

CATALOG_FILTERS = [
    {
        "name": "Категории",
        "type": "categories",
        "slug": "categories",
    },
    {
        "name": "Цена",
        "type": "range",
        "slug": "price",
    },
    {
        "name": "Условия хранения",
        "type": "checkbox",
        "slug": "store_conditions",
    },
    {
        "name": "Срок хранения",
        "type": "checkbox",
        "slug": "shelf_life",
    },
    {
        "name": "Температура хранения",
        "type": "checkbox",
        "slug": "store_temperature",
    },
    {
        "name": "Новинка",
        "type": "boolean",
        "slug": "is_new",
    },
    {
        "name": "Хит продаж",
        "type": "boolean",
        "slug": "is_trendy",
    },
]

# -- Jet Admin Configurations

# Тема по умолчанию
JET_DEFAULT_THEME = 'light-violet'

# Список тем
JET_THEMES = [
    {
        'theme': 'default', # theme folder name
        'color': '#47bac1', # color of the theme's button in user menu
        'title': 'Default' # theme title
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': 'Green'
    },
    {
        'theme': 'light-green',
        'color': '#2faa60',
        'title': 'Light Green'
    },
    {
        'theme': 'light-violet',
        'color': '#a464c4',
        'title': 'Light Violet'
    },
    {
        'theme': 'light-blue',
        'color': '#5EADDE',
        'title': 'Light Blue'
    },
    {
        'theme': 'light-gray',
        'color': '#222',
        'title': 'Light Gray'
    }
]
JET_SIDE_MENU_COMPACT = True

try:
    from .settings_override import *
except ImportError:
    pass
