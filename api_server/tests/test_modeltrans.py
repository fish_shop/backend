import pytest
from django.utils.translation import activate

from catalog.models import Category


@pytest.mark.skip
@pytest.mark.django_db
def test_create_simple():
    """
    Because the whole point of using the modeltranslation app is translating dynamic content,
        the fields marked for translation are somehow special when it comes to accessing them.
        The value returned by a translated field is depending on the current language setting.
        “Language setting” is referring to the Django set_language view and the corresponding get_lang function.

        https://docs.djangoproject.com/en/dev/topics/i18n/translation/#set-language-redirect-view
    """
    activate('ru')
    name = 'Русский заголовок'
    n = Category(name=name)
    n.save()

    actual = Category.objects.all()[0]
    assert actual.name == name
    assert actual.name_ru == name
    assert actual.name_en is None


@pytest.mark.skip
@pytest.mark.django_db
def test_create_all_langs():
    activate('ru')
    name_ru = 'Русский заголовок'
    name_en = 'English name'
    n = Category(name_ru=name_ru, name_en=name_en)
    n.save()

    actual = Category.objects.all()[0]
    assert actual.name == name_ru
    assert actual.name_ru == name_ru
    assert actual.name_en == name_en


@pytest.mark.skip
@pytest.mark.django_db
def test_set_simple():
    activate('ru')
    name_ru = 'Первоначальный заголовок'
    name_en = 'Initial name'
    name_ru_new = 'Новый заголовок'

    n = Category(name_ru=name_ru, name_en=name_en)
    n.name = name_ru_new
    n.save()

    actual = Category.objects.all()[0]

    assert actual.name == name_ru_new
    assert actual.name_ru == name_ru_new
    assert actual.name_en == name_en
